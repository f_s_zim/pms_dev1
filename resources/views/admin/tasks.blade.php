@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title ">Completed tasks list</h3>
            <p class="card-category"> Total {{$completedTasks->count()}} tasks completed</p>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table">
                <thead class=" text-primary">
                <th>
                    Tasks
                </th>
                <th>
                    Assigned Employee
                </th>
                <th>
                    Project
                </th>
                <th>
                    Priority
                </th>
                </thead>
                <tbody>
                    @foreach($completedTasks as $completedTask)
                        <tr>
                            <td>
                                <h4>{{$completedTask->title}}</h4>
                            </td>
                            <td>
                                @if($completedTask->user)
                                    {{$completedTask->user->name}}
                                @else
                                    <p class="text-warning">Employee not assigned yet</p>
                                @endif
                                
                            </td>
                            <td>
                                {{$completedTask->project->name}}
                            </td>
                            <td>
                                @if($completedTask->priority == 1)
                                    <h6 class="text-danger">HIGH</h6>
                                {{--  @endif  --}}
                                @elseif($completedTask->priority == 2)
                                    <h6 class="text-warning">Medium</h6>
                                @elseif($completedTask->priority == 3)
                                    <h6 class="text-info">LOW</h6>
                                {{--  @endif  --}}
                                @elseif($completedTask->priority == 0)
                                    <h6 class="text-primary">Priority not set yet</h6>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
        <div class="card-header card-header-primary">
            <h3 class="card-title ">Completed tasks list</h3>
            <p class="card-category"> Total {{$dueTasks->count()}} tasks completed</p>
        </div>
        <div class="card-body">
            <div class="table-responsive">
            <table class="table">
                <thead class=" text-primary">
                <th>
                    Tasks
                </th>
                <th>
                    Assigned Employee
                </th>
                <th>
                    Project
                </th>
                <th>
                    Priority
                </th>
                </thead>
                <tbody>
                    @foreach($dueTasks as $dueTask)
                        <tr>
                            <td>
                                <h4>{{$dueTask->title}}</h4>
                            </td>
                            <td>
                                @if($dueTask->user)
                                    {{$dueTask->user->name}}
                                @else
                                    <p class="text-warning">Employee not assigned yet</p>
                                @endif
                                
                            </td>
                            <td>
                                {{$dueTask->project->name}}
                            </td>
                            <td>
                                @if($dueTask->priority == 1)
                                    <h6 class="text-danger">HIGH</h6>
                                {{--  @endif  --}}
                                @elseif($dueTask->priority == 2)
                                    <h6 class="text-warning">Medium</h6>
                                @elseif($dueTask->priority == 3)
                                    <h6 class="text-info">LOW</h6>
                                {{--  @endif  --}}
                                @elseif($dueTask->priority == 0)
                                    <h6 class="text-primary">Priority not set yet</h6>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>
</div>


@endsection