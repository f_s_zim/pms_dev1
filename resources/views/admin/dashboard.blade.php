@extends('layouts.admin')

@section('content')
<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">group</i>
                </div>
                <p class="card-category">Total Employees</p>
                <h3 class="card-title">
                    {{$employees->count()}}
                {{-- <small>GB</small> --}}
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                        <i class="material-icons">person_add</i>
                <a href="#pablo">Hire more Employees</a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
        <div class="card-header card-header-success card-header-icon">
            <div class="card-icon">
                <i class="material-icons">content_paste</i>
            </div>
            <p class="card-category">Total Projects</p>
            <h3 class="card-title">{{ $projects->count() }}</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">date_range</i> Last 24 Hours
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
        <div class="card-header card-header-danger card-header-icon">
            <div class="card-icon">
                <i class="material-icons">assignment</i>
            </div>
            <p class="card-category">Total Tasks</p>
            <h3 class="card-title">{{ $tasks->count() }}</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">local_offer</i> Tracked from Github
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
        <div class="card-header card-header-info card-header-icon">
            <div class="card-icon">
                <i class="material-icons">assignment_turned_in</i>
            </div>
            <p class="card-category">Completed Tasks</p>
            <h3 class="card-title">{{$completedTasks->count()}}</h3>
        </div>
        <div class="card-footer">
            <div class="stats">
            <i class="material-icons">update</i> Just Updated
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
