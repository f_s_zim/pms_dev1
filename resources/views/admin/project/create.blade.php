
@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Create Project</h4>
                    <p class="card-category">Complete your project details</p>
                </div>
                <div class="card-body">
                <form method="POST">
                    
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Project Name</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="selectPriority">Priority</label>
                                <select class="form-control" id="selectPriority" name="priority">
                                    <option value=1>High</option>
                                    <option value=2>Medium</option>
                                    <option value=3>Low</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Deadline</label>
                        <input id="selectedDeadline" class="form-control" type="text" name="deadline"/>
                    </div>


                    <div class="form-group">
                        <label class="bmd-label-floating">Project Description</label>
                        <textarea name="description" id="projectDescription" cols="30" rows="10"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    <div class="clearfix"></div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection