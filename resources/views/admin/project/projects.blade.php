@extends('layouts.admin')

@section('content')

<div class="col-md-12">
    <div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title ">Project List</h4>
        <p class="card-category"> Total projects: {{$projects->count()}}</p>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead class=" text-primary">
                <th></th>
                <th></th>
                <th></th>
                <th>
                    Name
                </th>
                <th>
                    Priority
                </th>
                <th></th>
                <th></th>
                <th></th>
                <th>
                    Deadline
                </th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="float: right;">Actions</th>
            </thead>
            <tbody>
                @foreach($projects as $project)
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <p style="font-size: 15px;"> {{$project->name}} </p>
                        </td>
                        <td>
                            @if($project->priority == 1)
                                <h6 class="text-danger">High</h6>
                            @endif
                            @if($project->priority == 2)
                                <h6 class="text-warning">Medium</h6>
                            @endif
                            @if($project->priority == 3)
                                <h6 class="text-info">Low</h6>
                            @endif
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            {{ \Carbon\Carbon::parse($project->deadline)->format('j F, Y')  }}
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td >
                            <div style="display:table-cell; vertical-align:middle;">
                                <a class="btn btn-primary" href="{{url('/admin/project/'.$project->id)}}">View</a>
                            </div>
                            
                        </td>
                        <td>
                            @if(! $project->completed)
                                <div style="display:table-cell; vertical-align:middle;">
                                    <form method="post">
                                        @csrf
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                        <input type="submit" class="btn btn-success" value="Mark As Complete">
                                    </form>
                                </div>
                            @else
                                <div style="display:table-cell; vertical-align:middle;">
                                    <h4><span class="badge badge-info status-box">Completed</span></h4>
                                </div>
                            @endif   
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    </div>
</div>

<style>
    .status-box {
        margin-top: 5px;
        margin-bottom: -2px;
        padding-top: 14px;
        padding-bottom: 14px;
        padding-left: 55px;
        padding-right: 55px;
    }
</style>

@endsection