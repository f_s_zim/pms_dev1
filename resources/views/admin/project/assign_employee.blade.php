@extends('layouts.admin')

@section('content')

<div class="row">
    {{--  =============================================== Assignale Employees ===============================================  --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Assign Employee on project {{ $project->name }}</h4>
                <p class="card-category"> Add multiple employees on your project</p>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <th>
                                Name
                            </th>
                            <th><span style="float:right;">Action</span></th>
                        </thead>
                        <tbody id="assignableEmployeeList">
                            @foreach($assignableEmployees as $assignableEmployee)
                                <tr>
                                    <td>
                                        {{$assignableEmployee->name}}
                                    </td>
                                    <td>
                                        <span style="float:right;"><input name="selector[]" id="ad_Checkbox4" class="ads_Checkbox" type="checkbox" value="{{ $assignableEmployee->id }}" /></span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <input id="projectID" type="hidden" value="{{ $project->id }}">
                    @if(count($assignableEmployees))
                        <input style="float:right;" class="btn btn-primary" type="button" id="assign" name="save_value" value="Save" />
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{--  =============================================== Assigned Employees ===============================================  --}}
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title ">Assigned employees on {{$project->name}} </h4>
                @if(!$project->users->count())
                    <p class="card-category"> Employees not assigned yet</p>    
                @else
                    <p class="card-category"> {{$project->users->count()}} employees assigned</p>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead class=" text-primary">
                        <th>
                            Name
                        </th>
                    </thead>
                    <tbody id="assignedEmployee">
                        @foreach($assignedEmployees as $assignedEmployee)
                        <tr>
                            <td>
                                {{$assignedEmployee->name}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection