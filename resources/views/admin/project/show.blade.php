@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title "> {{ $project->name }} </h4>
                <p class="card-category"></p>
            </div>
            <div class="card-body">
                <p>{!! $project->description !!}</p>  
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                
                <div class="table-responsive">
                    <table id="employeeList" class="table">
                        <thead class=" text-info">
                            <th>
                                <div class="row">
                                    <div class="col-md-10">
                                        <h6 class="text-info">Assigned Employees</h6>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="dropdown">
                                            <button id="selectEmployee" type="button" class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="material-icons">person_add</i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <ul id="selectAssignableEmployees">
                                                    {{-- @foreach($users as $user)
                                                        <li href="#" class="dropdown-item employee" data-employee-id="{{ $user->id }}" data-project-id="{{ $project->id }}">{{$user->name}}</li>
                                                    @endforeach --}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </th>
                            <th>
                                
                            </th>
                        </thead>
                        <tbody id="assignedEmployeeList">
                            {{-- @foreach($assignedEmployees as $assignedEmployee)
                            <tr>
                                <td>
                                    {{$assignedEmployee->name}}
                                </td>
                            </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title "> Tasks on {{ $project->name }} </h4>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                        {{--  <table id="taskTable" class="table">  --}}
                            <table class="table">
                            <tbody>
                                
                                {{--  ============================== ADD TASK ==============================  --}}
                                <tr>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <td>
                                                <div>
                                                    <i class="material-icons">assignment</i>
                                                </div>
                                            </td>
                                        </div>
                                        <div class="col-md-8">
                                            <td>
                                                <input id="task" class="form-control" type="text">
                                            </td>
                                        </div>
                                        <td></td>
                                        <div class="col-md-2">
                                            <td class="td-actions text-right">
                                                <input id="projectID" type="hidden" value="{{ $project->id }}">
                                                <button id="addTask" class="btn btn-outline-success btn-circle-lg"><i class="material-icons md-24">add</i></button>
                                            </td>
                                        </div>
                                        <td></td>
                                    </div>
                                    

                                </tr>
                            </tbody>
                        </table>
                        <table id="taskTable" class="table">
                            <tbody>
                                <th></th>
                                <th>Tasks</th>
                                {{--  <th class="text-danger">Deadline</th>  --}}
                                <th>Employee</th>
                                <th></th>
                                <th>Actions</th>
                            </tbody>
                            <tbody id="allTasksContainer">
                                
                            </tbody>
                        </table>

                        {{-- Delete Task and edit task Modal --}}
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to delete the task?</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    Once you delete the task, no one will see it again.
                                    </div>
                                    <div class="modal-footer">
                                        <button id="deleteTask" type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="myLargeModalLabel">Edit Task</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="titleField" class="form-group">
                                                        <label class="bmd-label-floating">Task Title</label>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="descriptionField" class="form-group">
                                                <label class="bmd-label-floating">Task Description</label>
                                                <textarea name="description" id="taskDescription" cols="30" rows="10" ></textarea>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="editButton" type="button" class="btn btn-primary" data-dismiss="modal">Update</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .btn-circle {
        width: 30px;
        height: 30px;
        padding: 6px 0px;
        border-radius: 15px;
        text-align: center;
        font-size: 12px;
        line-height: 1.42857;
    }

    .btn-circle-lg {
        width: 50px;
        height: 50px;
        padding: 6px 0px;
        border-radius: 60px;
        text-align: center;
        font-size: 12px;
        line-height: 1.42857;
    }

    .btn-space {
        margin-right: 2px;
        padding-right: 2px;
    }
        
</style>

@stop