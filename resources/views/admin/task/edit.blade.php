
@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Edit Task</h4>
                    <p class="card-category">Edit your task details.</p>
                </div>
                <div class="card-body">
                <form method="POST">
                    
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="bmd-label-floating">Task title</label>
                                <input type="text" class="form-control" name="title" value="{{ $task->title }}">
                            </div>
                        </div>
                    </div>


                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="selectPriority">Priority</label>
                                <select class="form-control" id="selectPriority" name="priority">
                                    <option value=1>High</option>
                                    <option value=2>Medium</option>
                                    <option value=3>Low</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}

                    {{-- <div class="form-group">
                        <label for="">Deadline</label>
                        <input id="selectedDeadline" class="form-control" type="text" name="deadline"/>
                    </div> --}}


                    <div class="form-group">
                        <label class="bmd-label-floating">Task description</label>
                        <textarea name="description" id="taskescription" cols="30" rows="10">{!! $task->description !!}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection