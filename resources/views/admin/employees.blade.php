@extends('layouts.admin')

@section('content')

<div class="col-md-12">
    <div class="card">
    <div class="card-header card-header-primary">
        <h4 class="card-title ">Simple Table</h4>
        <p class="card-category"> Here is a subtitle for this table</p>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table">
            <thead class=" text-primary">
                <th>
                    Name
                </th>
                <th>
                    Job Title
                </th>
                <th>
                    Total Projects
                </th>
                <th>
                    Total Tasks
                </th>
                <th>
                    Completed Tasks
                </th>
                <th>
                    Due Tasks
                </th>
            </thead>
            <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>
                            {{$employee->name}}
                        </td>
                        <td>
                            JOB TITLE
                        </td>
                        <td>
                            {{$employee->projects->count()}}
                        </td>
                        <td>
                            {{$employee->tasks->count()}}
                        </td>
                        <td>
                            {{$employee->completedTasks()->count()}}
                        </td>
                        <td>
                            {{$employee->dueTasks()->count()}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
    </div>
    </div>
</div>

@endsection