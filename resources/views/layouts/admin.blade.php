<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Project Management System</title>

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="{{ asset('assets/admin/assets/css/material-dashboard.css?v=2.1.0') }}" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" /> 
    
    {{--  <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />  --}}
    {{--  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />  --}}
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/admin/assets/demo/demo.css') }}" rel="stylesheet" />
    {{--  custom-CSS  --}}
    {{-- <link rel="stylesheet" type="text/css" href="asset('assets/css/style.css')"/> --}}
</head>
<body>
    
    <div class="wrapper">
        @include('partials.admin.leftsidebar')
        <div class="main-panel">
            @include('partials.admin.pageheader')
            <div class="content">
                <div class="container-fluid">
                    @include('partials.admin.sessionMessages.session_messages')
                </div>
                @yield('content')
            </div>
            @include('partials.admin.footer')
        </div>
    </div>


    
    <script src="{{ asset('assets/admin/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/assets/js/core/bootstrap-material-design.min.js') }}" type="text/javascript"></script>
    {{--  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>  --}}
    <script src="{{ asset('assets/admin/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>

    {{--  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>  --}}
    <script src="{{ asset('assets/admin/assets/js/plugins/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/admin/assets/js/plugins/bootstrap-notify.js') }}"></script>
    
    <script src="{{ asset('assets/admin/assets/js/material-dashboard.min.js?v=2.1.0') }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script> 
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    {{--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>  --}}
    
    {{--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>  --}}
    {{--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>  --}}
    <script src="{{asset('assets/admin/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    
    {{-- --------------------- Custom Js --------------------- --}}
    <script src="{{ asset('assets/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/assign-employees.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/task.js') }}" type="text/javascript"></script>

    

</body>
</html>