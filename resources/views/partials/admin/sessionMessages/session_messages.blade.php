@if (session('success'))
    
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="material-icons">close</i>
        </button>
        <span>
            <b> Success - </b> {{ session('success') }}
        </span>
    </div>

@endif

@if ($errors->any())
    
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <i class="material-icons">close</i>
        </button>
        @foreach ($errors->all() as $error)
            <span>
                <b> Danger - </b> {{ $error }}
            </span>
        @endforeach
    </div>

@endif