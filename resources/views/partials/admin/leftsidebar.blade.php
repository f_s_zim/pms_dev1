<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('/sidebar/sidebar-1.jpg')}}">

  {{-- Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

  Tip 2: you can also add an image using data-image tag --}}

<div class="logo">
  <a href="#" class="simple-text logo-normal">
    Brand Name
  </a>
</div>
<div class="sidebar-wrapper">
  <ul class="nav">
    <li class="nav-item active  ">
      <a class="nav-link" href="{{url('/admin/dashboard')}}">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
      </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="./notifications.html">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
        </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="{{url('admin/profile')}}">
        <i class="material-icons">person</i>
        <p>User Profile</p>
      </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/admin/employees')}}">
            <i class="material-icons">group</i>
            <p>Employee List</p>
        </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="{{url('/admin/projects')}}">
        <i class="material-icons">assignment</i>
        <p>Project List</p>
      </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/admin/create-project')}}">
          <i class="material-icons">content_paste</i>
          <p>Create Project</p>
        </a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="{{url('/admin/tasks')}}">
        <i class="material-icons">library_books</i>
        <p>Task List</p>
      </a>
    </li>
    {{-- <li class="nav-item ">
        <a class="nav-link" href="{{url('/admin/create-task')}}">
          <i class="material-icons">content_paste</i>
          <p>Create Task</p>
        </a>
    </li> --}}
  </ul>
</div>
</div>