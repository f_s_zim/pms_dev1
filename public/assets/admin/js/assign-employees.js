(function ($) {

    function updateAssignableEmployeesUI (assignableEmployees) {
        let employeeList = '';

        for (let i = 0; i < assignableEmployees.length; i++) {
            
            let employee = '';
            
            employee +=    '<tr>';
            employee +=        '<td>';
            employee +=            assignableEmployees[i].name;
            employee +=        '</td>';
            employee +=        '<td>';
            employee +=            '<span style="float:right;"><input name="selector[]" id="ad_Checkbox4" class="ads_Checkbox" type="checkbox" value='+assignableEmployees[i].id+' /></span>';
            employee +=        '</td>';
            employee +=    '</tr>';

            employeeList += employee;
        }

        $('#assignableEmployeeList').html(employeeList);
    }

    function updateAssignedEmployeesUI (assignedEmployees) {
        let employeeList = '';

        for (let i = 0; i < assignedEmployees.length; i++) {
            
            let employee = '';
            employee += '<tr>';
            employee +=    '<td>';
            employee +=        assignedEmployees[i].name;
            employee +=    '</td>';
            employee += '</tr>';
            employeeList += employee;
        }

        $('#assignedEmployee').html(employeeList);
    }

    function getAndUpdateEmployeeList(project_id) {

        $.ajax({
            metthod: "get",
            url: '/admin/assign-employees/'+project_id+'',
        
        }).done(function (data) {
            
            updateAssignableEmployeesUI(data.assignableEmployees);
            updateAssignedEmployeesUI(data.assignedEmployees);
            console.log(data.assignableEmployees);
            console.log(data.assignedEmployees);

        }).fail(function (data) {
            console.log('failed');
        });
    }
    
    function assignEmployee (employees, projectID) {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/assign-employees",
            data: {

                employees : employees,
                projectID : projectID
            
            }

        }).done(function (data) {
            // updatetUI();
            getAndUpdateEmployeeList(projectID);
            console.log(data);
        
        }).fail(function (data) {
            console.log('failed');
            console.log(data);
        });
    }

    $(document).ready(function () {
        // let projectID = $('#projectID').val();
        // getAndUpdateEmployeeList(projectID);
        $('#assign').click(function () {
            let employees = [];
            let projectID = $('#projectID').val();
            $(':checkbox:checked').each(function(i){
                employees[i] = $(this).val();
            });

            assignEmployee(employees, projectID);
        });
    });

})(jQuery);