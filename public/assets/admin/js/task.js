(function ($) {

    function getAndUpdateTaskUI(tasks, employees, assignableEmployees) {
        let taskList = '';
        
        for(let i=0; i < tasks.length; i++) {
            let singleTask = '';

            // ============================= Check Button =============================
            singleTask += '<tr>';
            singleTask +=    '<td>';
            singleTask +=        '<div class="form-check">';
            singleTask +=        '<span class="btn-space">';

            if (tasks[i].priority == 0) {
                singleTask +=            '<button id="completeButton'+i+'" type="button" class="btn btn-outline-primary btn-circle markAsComplete" data-task-id='+tasks[i].id+'>';
                singleTask +=                '<i class="material-icons">check</i>';
                singleTask +=            '</button>';
            }

            if (tasks[i].priority == 1) {
                singleTask +=            '<button id="completeButton'+i+'" type="button" class="btn btn-outline-danger btn-circle markAsComplete" data-task-id='+tasks[i].id+'>';
                singleTask +=                '<i class="material-icons">check</i>';
                singleTask +=            '</button>';
            }

            if (tasks[i].priority == 2) {
                singleTask +=            '<button id="completeButton'+i+'" type="button" class="btn btn-outline-warning btn-circle markAsComplete" data-task-id='+tasks[i].id+'>';
                singleTask +=                '<i class="material-icons">check</i>';
                singleTask +=            '</button>';
            }

            if (tasks[i].priority == 3) {
                singleTask +=            '<button id="completeButton'+i+'" type="button" class="btn btn-outline-info btn-circle markAsComplete" data-task-id='+tasks[i].id+'>';
                singleTask +=                '<i class="material-icons">check</i>';
                singleTask +=            '</button>';
            }
            
            
            singleTask +=        '</span>';
            singleTask +=        '</div>';
            singleTask +=    '</td>';

            // ============================= Task Title =============================
            singleTask +=    '<td>';
            singleTask +=       '<p style="font-size:18px;">'+tasks[i].title+'</p>';
            singleTask +=    '</td>';

            // ============================= Set Deadline =============================
            // singleTask +=    '<td>';
            // singleTask +=       '<input id="datepicker'+i+'" class="form-control datepicker" width="150" />';
            // singleTask +=       '<input type="text" class="form-control"/>';
            // singleTask +=    '</td>';
            // ============================= Assigned Employee =============================
            singleTask +=    '<td>';

            if (!tasks[i].user_id) {
                singleTask += '<h6>Employee not assigned</h6>';
            } else {
                for (let k = 0; k < employees.length; k++) {
                    console.log(employees[k].name, employees.length);
                    if (employees[k].id == tasks[i].user_id) {
                        console.log(employees[k].name, '|', employees[k].id, '|', tasks[i].user_id);
                        singleTask += '<h6>'+employees[k].name+'</h6>';
                    }
                }
            }

            singleTask +=    '</td>';
            
            // ============================= Edit Task =============================
            singleTask +=    '<td></td>';
            singleTask +=    '<td class="td-actions text-right">';
            singleTask +=        '<span class="btn-space">';
            // singleTask +=            '<button id="editButton'+i+'" type="button" rel="tooltip" title="Edit Task" class="btn btn-outline-primary btn-circle editButton" data-toggle="modal" data-target=".bd-example-modal-lg" data-task-id='+tasks[i].id+'>';
            singleTask +=            '<a href="'+window.location.origin+'/admin/task/edit/'+tasks[i].id+'"><button id="editButton'+i+'" type="button" class="btn btn-outline-primary btn-circle editButton" data-task-id='+tasks[i].id+'>';
            singleTask +=                '<i class="material-icons">edit</i>';
            singleTask +=            '</button></a>';
            singleTask +=        '</span>';

            // ============================= Assign Employee =============================
            singleTask +=        '<span class="btn-space">';
            singleTask +=           '<div class="dropdown">';
            singleTask +=               '<button type="button" class="btn btn-outline-primary btn-circle dropdown-toggle assignmentButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
            singleTask +=                   '<i class="material-icons">person_add</i>';
            singleTask +=               '</button>';
            singleTask +=               '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
            singleTask +=                   '<ul >';

            for (let j=0; j < assignableEmployees.length; j++) {
                singleTask +=                   '<li id="employeeID'+i+''+j+'" href="#" class="dropdown-item employee" data-employee-id='+assignableEmployees[j].id+' data-task-id='+tasks[i].id+'>'+assignableEmployees[j].name+'</li>';
            }
            
            singleTask +=                   '</ul>';
            singleTask +=               '</div>';
            singleTask +=           '</div>';
            singleTask +=        '</span>';
            
            // ============================= Priority =============================

            singleTask +=        '<span class="btn-space">';
            singleTask +=           '<div class="dropdown">';
            singleTask +=               '<button type="button" class="btn btn-outline-primary btn-circle dropdown-toggle assignmentButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
            singleTask +=                   '<i class="material-icons">outlined_flag</i>';
            singleTask +=               '</button>';
            singleTask +=               '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
            singleTask +=                   '<ul >';
            singleTask +=                       '<li id="priorityLevel1" href="#" class="dropdown-item priority" data-priority-level="1" data-task-id='+tasks[i].id+'>High</li>';
            singleTask +=                       '<li id="priorityLevel2" href="#" class="dropdown-item priority" data-priority-level="2" data-task-id='+tasks[i].id+'>Medium</li>';
            singleTask +=                       '<li id="priorityLevel3" href="#" class="dropdown-item priority" data-priority-level="3" data-task-id='+tasks[i].id+'>Low</li>';
            singleTask +=                   '</ul>';
            singleTask +=               '</div>';
            singleTask +=           '</div>';
            singleTask +=        '</span>';
                        
            // ============================= Delete Task =============================
            singleTask +=        '<span class="btn-space">';
            singleTask +=            '<button id="deleteButton'+i+'" type="button" class="btn btn-outline-danger btn-circle deleteButton" data-task-id='+tasks[i].id+' data-toggle="modal" data-target="#exampleModal">';
            singleTask +=                '<i class="material-icons">close</i>';
            singleTask +=            '</button>';
            singleTask +=        '</span>';
            singleTask +=    '</td>';
            singleTask += '</tr>';
            
            taskList += singleTask;
        }

        
        $('#allTasksContainer').html(taskList);


        /*
        $('#taskTable input').trigger('click');

        $('#taskTable input').datepicker({
        });
        */
    }

    function getAndUpdateEmployeeListUI(employees) {
        let employeeList = '';

        for (let i=0; i < employees.length; i++) {
            let employee = '';
            employee += '<tr>';
            employee +=     '<td>';
            employee +=         employees[i].name;
            employee +=     '</td>';
            employee +=     '<td>';
            employee +=        '<span class="btn-space">';
            employee +=            '<button id="removeButton'+i+'" type="button" class="btn btn-outline-danger btn-circle removeButton" data-employee-id='+employees[i].id+'>';
            employee +=                '<i class="material-icons">close</i>';
            employee +=            '</button>';
            employee +=        '</span>';
            employee +=     '</td>';
            employee += '</tr>';

            employeeList += employee;
        }

        $('#assignedEmployeeList').html(employeeList);

    }

    function getAndUpdateAssignableEmployeesUI(employees , projectID) {
        let employeeList = '';
        for(let i=0; i < employees.length; i++) {
            let employee = '';
            employee += '<li href="#" class="dropdown-item employee" data-employee-id="'+employees[i].id+'" data-project-id="'+projectID+'"> '+employees[i].name+' </li>';
            employeeList += employee;                                           
        }

        $('#selectAssignableEmployees').html(employeeList);
    }

    function getTasks(projectID) {

        $.ajax({
            method: "get",
            url: "/admin/projects/"+projectID+"/tasks"

        }).done(function(data) {
            getAndUpdateTaskUI(data.tasks, data.employees, data.assignableEmployees);
            console.log(data.tasks);
        }).fail(function(data) {
            console.log('failed');
        });
    }

    function getAssignedEmployees(projectID) {
        $.ajax({
            method: "get",
            url: '/admin/project/'+projectID+'/assigned-employees',

        }).done(function(data) {
            console.log(data);
            getAndUpdateEmployeeListUI(data.assignedEmployees);
        }).fail(function() {
            console.log('failed');
        });
    }

    function getAssignableEmployees(projectID) {
        $.ajax({
            method: "get",
            url: '/admin/project/'+projectID+'/assignable-employees',

        }).done(function(data) {
            console.log(data);
            getAndUpdateAssignableEmployeesUI(data.assignableEmployees , projectID);
            getTasks(projectID);
        }).fail(function() {
            console.log('failed');
        });
    }

    function removeEmployee (employeeID, projectID) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            method: "delete",
            url: '/admin/project/'+projectID+'/remove-rmployee',
            data: {
                employeeID: employeeID,
                projectID: projectID
            }
        }).done(function() {
            getAssignedEmployees(projectID);
            getTasks(projectID);
        }).fail(function() {
            console.log('failed to remove');
        });
    }
    
    /*
    function updateEditForm (title, description) {
        let taskTitle = '<input id="taskTitle" type="text" class="form-control" value='+title+' name="title"  />';
        $('#titleField').html(taskTitle);
        if (!description) {
            let taskDescription = '<textarea name="description" id="taskDescription" cols="30" rows="10" >'+description+'</textarea>';
            $('#descriptionField').html(taskDescription);    
        } else {
            let taskDescription = '<textarea name="description" id="taskDescription" cols="30" rows="10" placeholder="Write description"></textarea>';
            $('#descriptionField').html(taskDescription);
        }

        if($('#taskDescription').length){
            tinymce.init({
                selector: '#taskDescription',
                element_format : 'html',
                verify_html: false,
                plugins: [
                    "advlist anchor autolink code codesample colorpicker contextmenu fullscreen help image imagetools", " lists link media noneditable preview", " searchreplace table template textcolor" +
                    " visualblocks wordcount"
                ]
            });
        }
        
        
        
        
        console.log(description);
    }

    function getTaskByID(taskID) {
        // let task = [];
        $.ajax({
            method: "get",
            url: "/admin/task/"+taskID

        }).done(function(data) {
            // task.push(data.task.title);
            // task.push(data.task.description);
            console.log(data.task.title, data.task.description);
            updateEditForm(data.task.title, data.task.description);
        }).fail(function(data) {
            console.log('failed');
        });

        // return task;
    }

    */

    function addTask (task, projectID) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/create-task",
            data: {
                task: task,
                projectID: projectID
            }

        }).done(function (data) {
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log('failed');
        });
    }

    function assignEmployeeOnTask(employeeID, taskID, projectID) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/task/assign-employee",
            data: {
                employeeID: employeeID,
                taskID: taskID,
                projectID: projectID
            }

        }).done(function (data) {
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('failed');
        });
    }

    function assignEmployeeOnProject(employeeID, projectID) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/project/assign-employee",
            data: {
                employeeID: employeeID,
                projectID: projectID
            }

        }).done(function (data) {
            getAssignedEmployees(projectID);
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('failed');
        });
    }
    function setPriorityLevel (priorityLevel, taskID, projectID) {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/task/set-priority",
            data: {
                priorityLevel: priorityLevel,
                taskID: taskID
            }

        }).done(function (data) {
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('failed');
        });
    }

    function deleteTask (taskID, projectID) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "delete",
            url: "/admin/task/delete",
            data: {
                taskID: taskID
            }

        }).done(function (data) {
            console.log('Task deleted!')
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('delete failed');
        });
    }

    function editTask (title, description, taskID, projectID) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/task/edit",
            data: {
                title: title,
                description: description,
                taskID: taskID
            }

        }).done(function (data) {
            console.log('Task edited!');
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('edit failed');
        });
    }

    function markAsComplete (taskID, projectID) {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: "post",
            url: "/admin/task/mark-as-complete",
            data: {
                taskID: taskID
            }

        }).done(function (data) {
            getTasks(projectID);
        
        }).fail(function (data) {
            console.log(data);
            console.log('failed');
        });
    }

    $(document).ready(function () {

        // Get initial Tasks
        let projectID = parseInt($('#projectID').val());
        getTasks(projectID);
        getAssignedEmployees(projectID);

        // Add Task
        $('#addTask').on('click', function () {
            let task = $('#task').val();    
            addTask(task, projectID);
            $('#task').val('');
        });

        // Mark as complete
        $('#taskTable').on('click', 'button.markAsComplete', function(){
            let taskID = $(this).data('task-id');
            markAsComplete (taskID, projectID);
            console.log('mark as complete event triggered');
        });

        // Edit Task
        
        /*
        $('#taskTable').on('click', 'button.editButton', function(){
            
            let taskID = $(this).data('task-id');
            // let task = getTaskByID(taskID);
            // console.log(task, task.length);

            getTaskByID(taskID);
            
            $("#taskTitle").attr("value",task[0]);
            $('#taskDescription').attr("value",task[1]);

            $('#editButton').on('click', function() {
                let title = $('#taskTitle').val();
                let description = $('textarea#taskDescription').val();
                editTask (title, description, taskID, projectID);
                console.log('edit event triggered!');
            });
        });

        */

        // Delete Task
        $('#taskTable').on('click', 'button.deleteButton', function(){
            let taskID = $(this).data('task-id');
            $('#deleteTask').on('click', function() {
                deleteTask (taskID, projectID);
                console.log('delete event triggered!');
            });
        });

        // Remove employee
        $('#employeeList').on('click', 'button.removeButton', function(){
            let employeeID = $(this).data('employee-id');
            removeEmployee (employeeID, projectID);
        });

        // Assign Employee to project
        $('#employeeList').on('click', 'li.employee', function(){
            let employeeID = $(this).data('employee-id');
            let projectID = $(this).data('project-id');
            assignEmployeeOnProject(employeeID, projectID);
        });

        // Assign Employee to task
        $('#taskTable').on('click', 'li.employee', function(){
            let employeeID = $(this).data('employee-id');
            let taskID = $(this).data('task-id');
            assignEmployeeOnTask(employeeID, taskID, projectID);
        });

        // Set Priority
        $('#taskTable').on('click', 'li.priority', function(){
            let priorityLevel = $(this).data('priority-level');
            let taskID = $(this).data('task-id');
            setPriorityLevel(priorityLevel, taskID, projectID);
        });

        $('#selectEmployee').on('click', function() {
            getAssignableEmployees(projectID);
        });
        // ========================= Set Deadline =========================
        /*
        $('#taskTable input').trigger('click');
        $('#taskTable').on('click', 'input.datepicker', function() {

            let datepickerID = $(this).attr('id');
            $('#'+datepickerID).datepicker({
                showOtherMonths: true
            });
        });
        */

        // ========================== Backup ============================
        /*
        $('#taskTable input.datepicker').datepicker({
            showOtherMonths: true
        });
        $('#taskTable').on('click', 'input', function() {
            
            let datepickerID = $(this).attr('id');
            $('#'+datepickerID).datepicker({
                // showOtherMonths: true
            });
        });
        */
        
    });

})(jQuery);