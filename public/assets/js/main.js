$(function() {
    "use strict";
    $(document).ready(function(){

        // $('#selectedDeadline').daterangepicker({
        //     opens: 'left'
        //     }, function(start, end, label) {
        //         console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        //     });

        $('#selectedDeadline').daterangepicker({
            "singleDatePicker": true,
            "showDropdowns": true,
            "startDate": moment().format('MM-DD-YYYY'),
            "endDate": moment().format('MM-DD-YYYY'),
            "minDate": moment().format('MM-DD-YYYY')
        }, function(start, end, label) {
            console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        });

        if($('#projectDescription').length){
            tinymce.init({
                selector: '#projectDescription',
                element_format : 'html',
                verify_html: false,
                plugins: [
                    "advlist anchor autolink code codesample colorpicker contextmenu fullscreen help image imagetools", " lists link media noneditable preview", " searchreplace table template textcolor" +
                    " visualblocks wordcount"
                ]
            });
        }

        if($('#taskDescription').length){
            tinymce.init({
                selector: '#taskDescription',
                element_format : 'html',
                verify_html: false,
                plugins: [
                    "advlist anchor autolink code codesample colorpicker contextmenu fullscreen help image imagetools", " lists link media noneditable preview", " searchreplace table template textcolor" +
                    " visualblocks wordcount"
                ]
            });
        }
    });
        
});