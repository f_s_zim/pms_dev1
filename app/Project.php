<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function tasks() {
        return $this->hasMany('App\Task');
    }

    // Check if the user assigned on the project or not
    public function assigned($user_id) {   
        $user = User::findOrFail($user_id);
        $projects = $user->projects()->where('project_id', $this->id)->get();

        if($projects->count()) {
            return true;
        } else {
            return false;
        }
    }

    // Return assignableEmployees
    public function assignableEmployees() {
        $employees = User::all();
        $assignableEmployees = [];

        // check if that employee assinged on that project or not and make list of $assignableEmployees.
        foreach($employees as $employee) {

            // List of assignableEmployees :
            if ( !$this->assigned($employee->id, $this->id) ) {
                array_push($assignableEmployees, $employee);
            }
        }

        return $assignableEmployees;
    }

    // Return assignedEmployees
    public function assignedEmployees() {
        $employees = User::all();
        $assignedEmployees = [];

        // check if that employee assinged on that project or not and make list of $assignedEmployees.
        foreach($employees as $employee) {

            // List of assignableEmployees :
            if ( $this->assigned($employee->id, $this->id) ){
                array_push($assignedEmployees, $employee);
            }
        }

        return $assignedEmployees;
    }
}
