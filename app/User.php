<?php

namespace App;

use App\Task;
use App\Project;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projects() {
        return $this->belongsToMany('App\Project');
    }

    public function tasks() {
        return $this->hasMany('App\Task');
    }

    public function completedTasks() {
        $tasks = Task::where('user_id', $this->id)->where('completed', true)->get();
        return $tasks;
    }

    public function dueTasks() {
        $tasks = Task::where('user_id', $this->id)->where('completed', false)->get();
        return $tasks;
    }
}
