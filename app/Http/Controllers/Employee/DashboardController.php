<?php 

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;

class DashboardController extends controller {

    public function getDashboard() {
        return view('employee.dashboard');
    }
}