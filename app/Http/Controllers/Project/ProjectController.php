<?php 

namespace App\Http\Controllers\Project;

use App\User;
use App\Task;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ProjectController extends controller {

    public function index() {
        $data['projects'] = Project::all();
        return view('admin.project.projects', $data);
    }

    
    public function show($id) {
        $project = Project::findOrFail($id);
        $users = $project->assignableEmployees($id);
        // $task = Task::all();
        // $assignableEmployees = $project->assignableEmployees($id);
        // $assignedEmployees = $project->assignedEmployees($id);
        
        // $data = [

        //     'assignableEmployees' => $project->assignableEmployees($id),
        //     'assignedEmployees' => $project->assignedEmployees($id),
        //     'project' => $project
        // ];

        $data['project'] = $project;
        $data['users'] = $users;
        return view('admin.project.show', $data);
    }

    public function create() {
        return view('admin.project.create');
    }

    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required|string',
            'priority' => 'integer',
            'deadline' => 'date',
            'description' => 'string',
        ]);

        $date = str_replace("/", "-", $request->deadline);
        $deadline = date_format(date_create_from_format('m-d-Y', $date), 'Y-m-d')." 23:59:59";


        $project = new Project;

        $project->name = $request->name;
        $project->priority = $request->priority;
        $project->deadline = $deadline;
        $project->description = $request->description;

        $project->save();

        return redirect('/admin/projects')->with('success', 'Project has been created successfully!');
    }

    public function getAssignEmployeesPage(Request $request, $id) {

        $project = Project::findOrFail($id);

        // If request is ajax then response json data else return specific view
        if ($request->ajax()) {
            
            return response()->json([
                'assignableEmployees' => $project->assignableEmployees(),
                'assignedEmployees' => $project->assignedEmployees(),
                'project' => $project
            ]);

        } else {

            $data = [

                'assignableEmployees' => $project->assignableEmployees(),
                'assignedEmployees' => $project->assignedEmployees(),
                'project' => $project
            ];

            return view('admin.project.assign_employee', $data);
        
        }
    }

    public function getAssignedEmployees($id) {

        $project = Project::findOrFail($id);
        $assignedEmployees = $project->assignedEmployees();
        return response()->json([
            'assignedEmployees' => $assignedEmployees
        ]);
    
    }

    public function getAssignableEmployees($id) {

        $project = Project::findOrFail($id);
        $assignableEmployees = $project->assignableEmployees();
        return response()->json([
            'assignableEmployees' => $assignableEmployees
        ]);
    
    }

    public function removeEmployeeFromProject(Request $request) {
        
        $project = Project::find($request->projectID);
        $project->users()->detach($request->employeeID);
        return response()->json([
            'success' => 'Employee removed from project!'
        ]);

    }

    public function assignEmployees(Request $request) {

        $employeeIDs = [];

        for($i = 0; $i < count($request->employees); $i++) {
            array_push($employeeIDs, (int) $request->employees[$i]); 
        }

        if ($request->ajax() && $request->has('employees') && $request->has('projectID')) {            
            $project = Project::find((int)$request->projectID);
            foreach($employeeIDs as $employeeID) {
                $project->users()->attach($employeeID);
            }

            return response()->json([
                '$employeeIDs' => $employeeIDs
            ]);
        }
    }

    public function assignEmployeeOnProject(Request $request) {
        $project = Project::find($request->projectID);
        $project->users()->attach($request->employeeID);
        return response()->json([
            'success' => 'Employee assigned successfully!'
        ]);
    }

    public function markAsComplete(Request $request) {
        $project = Project::find($request->project_id);
        $project->completed = 1;
        $project->save();

        return redirect()->back()->with('success', 'Project has been marked as completed!');
    }
}