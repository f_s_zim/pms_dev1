<?php 

namespace App\Http\Controllers\Task;

use App\User;
use App\Task;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TaskController extends controller {

    public function index(Request $request, $project_id) {
        
        if($request->ajax()) {

            $project = Project::find($project_id);
            // $tasks = $project->tasks;
            $tasks = Task::where('project_id', $project_id)->where('completed', false)->orderBy('priority', 'asc')->get();
            $assignableEmployees = $project->users;
            $employees = User::all();
            return response()->json([
                'tasks' => $tasks,
                'employees' => $employees,
                'assignableEmployees' => $assignableEmployees
            ]);
        }
        
        return redirect()->back();        
        
    }

    public function tasks() {
        $data['completedTasks'] = Task::where('completed', true)->get();
        $data['dueTasks'] = Task::where('completed', false)->get();
        // dd($data['completedTasks']);
        return view('admin.tasks', $data);
    }

    public function getTask($id) {

        $task = Task::find($id);
        return response()->json([
            'task' => $task
        ]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'task' => 'string|required'
        ]);

        $task = new Task;
        $task->project_id = $request->projectID;
        $task->title = $request->task;
        $task->save();

        return response()->json([
            'success' => 'Task created successfully!'
        ]);
    }

    public function assignEmployeeOnTask(Request $request) {

        $project = Project::find($request->projectID);
        $tasks = $project->tasks;
        $employees = $project->users;
        $employee = User::find($request->employeeID);

        $task = Task::find($request->taskID);
        $task->user_id = $request->employeeID;
        $task->save();

        return response()->json([
            'tasks' => $tasks,
            'employees' => $employees,
            // 'projectID' => $request->projectID,
            // 'employee' => $employee,
            'success' => 'Employee has been assigned!'
        ]);
    }

    public function setPriorityLevel(Request $request) {
        $task = Task::find($request->taskID);
        $task->priority = $request->priorityLevel;
        $task->save();
        return response()->json([
            // 'tasks' => $tasks,
            // 'employees' => $employees,
            // 'projectID' => $request->projectID,
            // 'employee' => $employee,
            'success' => 'Employee has been assigned!'
        ]);
    }

    public function markAsComplete(Request $request) {
        $task = Task::find($request->taskID);
        $task->completed = true;
        $task->save();

        return response()->json([
            'success' => 'Priority has been set successfully!'
        ]);
    }

    public function delete(Request $request) {
        $task = Task::find($request->taskID);
        $task->delete();

        return response()->json([
            'success' => 'Task deleted successfully!'
        ]);
    }

    // public function editTask(Request $request) {
    //     $task = Task::find($request->taskID);
    //     $task->title = $request->title;
    //     $task->description = $request->description;
    //     $task->save();

    // }

    public function edit($id) {
        $data['task'] = Task::find($id);
        return view('admin.task.edit', $data);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'string',
            'description' => 'string'
        ]);

        $task = Task::findOrFail($id);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->save();

        return redirect('/admin/project/'.$task->project_id)->with('Task edited successfully!');
    }
}