<?php 

namespace App\Http\Controllers\Admin;

use App\User;
use App\Task;
use App\Project;
use App\Http\Controllers\Controller;

class DashboardController extends controller {
    
    public function getDashboard() {
        
        $data = [
            'employees' => User::all(),
            'projects' => Project::all(),
            'tasks' => Task::all(),
            'completedTasks' => Task::where('completed', true)->get()
        ];
        return view('admin.dashboard', $data);
    }

    public function getEmployees() {
        $data['employees'] = User::all();
        return view('admin.employees', $data);
    }

    public function getTasks() {
        return view('admin.tasks');
    }

    public function getProfile() {
        return view('admin.profile');
    }
}