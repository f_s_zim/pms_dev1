<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfEmployee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
         if(Auth::user()->type == 'employee'){
         
             return $next($request);
         }
 
         if(Auth::user()->type == 'admin'){
             return redirect('/admin/dashboard');
         }
 
         return redirect('/login');
     }
}
