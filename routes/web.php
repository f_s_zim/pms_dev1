<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function(){

    // ----------- Only Employees can access these routes -----------
    Route::middleware(['employee'])->group( function() {
        Route::namespace('Employee')->group(function(){
            Route::get('/dashboard', 'DashboardController@getDashboard');
        });
    });

    // ----------- Only Admin can access these routes -----------
    Route::middleware(['admin'])->prefix('admin')->group( function() {
        Route::namespace('Admin')->group(function(){
            Route::get('/dashboard', 'DashboardController@getDashboard');
            Route::get('/employees', 'DashboardController@getEmployees');
            Route::get('/tasks', 'DashboardController@getTasks');
            Route::get('/profile', 'DashboardController@getProfile');    
        });

        Route::namespace('Project')->group(function(){
            Route::get('/project/{id}', 'ProjectController@show');
            Route::get('/projects', 'ProjectController@index');
            Route::post('/projects', 'ProjectController@markAsComplete');
            Route::get('/create-project', 'ProjectController@create');
            Route::post('/create-project', 'ProjectController@store');
            // Todo:  use slug for project
            Route::get('/assign-employees/{project_id}', 'ProjectController@getAssignEmployeesPage');
            Route::post('/assign-employees', 'ProjectController@assignEmployees');

            Route::get('/project/{id}/assigned-employees' , 'ProjectController@getAssignedEmployees');
            Route::get('/project/{id}/assignable-employees' , 'ProjectController@getAssignableEmployees');
            Route::post('/project/assign-employee', 'ProjectController@assignEmployeeOnProject');
            Route::delete('project/{id}/remove-rmployee', 'ProjectController@removeEmployeeFromProject');
        });

        Route::namespace('Task')->group(function() {
            Route::get('/tasks', 'TaskController@tasks');
            Route::get('/projects/{project_id}/tasks', 'TaskController@index');
            Route::get('/task/{id}', 'TaskController@getTask');
            Route::post('/create-task', 'TaskController@store');
            Route::post('/task/assign-employee', 'TaskController@assignEmployeeOnTask');
            Route::post('/task/set-priority', 'TaskController@setPriorityLevel');
            Route::post('/task/mark-as-complete', 'TaskController@markAsComplete');
            Route::delete('/task/delete', 'TaskController@delete');
            // Route::post('/task/edit', 'TaskController@editTask');
            Route::get('/task/edit/{id}', 'TaskController@edit');
            Route::post('/task/edit/{id}', 'TaskController@update');
        });
    });
    
});